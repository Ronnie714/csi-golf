﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    Vector2 start, end;
    bool ready;
    public Rigidbody rb;

    // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonUp(0) && ready) {
            end = Input.mousePosition;
            Vector2 dif = start - end;
            float mag = dif.magnitude;
            dif.Normalize();
            mag = Mathf.Min(mag, 36);
            dif = dif * mag;

            rb.AddForce(dif * 10);
            ready = false;
        }
        
    }

    void OnMouseDown() {
        //if (rb.velocity.magnitude < 0.25) {
            start = Input.mousePosition;
            ready = true;
        //}
       
    }
}
