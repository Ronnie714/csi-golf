﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishBlock : MonoBehaviour {
    [SerializeField]
    public GameObject Player;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }
    void OnCollisionEnter(Collision collision) {
        foreach (ContactPoint contact in collision.contacts)  {
            if (collision.gameObject == Player) {
                Player.GetComponent<Rigidbody>().velocity = new Vector2(0, 0);
                Player.transform.position = new Vector2(-5, 2);
            }
            
        }
        
    }
}
