﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public RectTransform image;
    float count = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        count+=0.1f;
        image.sizeDelta = new Vector2(1280 + (1280/12 * (Mathf.Sin(count))), 720 + (720/12 * (Mathf.Sin(count))));

    }
}
