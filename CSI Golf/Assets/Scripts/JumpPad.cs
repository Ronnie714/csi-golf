﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPad : MonoBehaviour {
    [SerializeField]
    public GameObject Player;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }
    void OnCollisionEnter(Collision collision) {
        foreach (ContactPoint contact in collision.contacts)  {
            if (collision.gameObject == Player) {
                Rigidbody rb = Player.GetComponent<Rigidbody>();
                rb.AddForce(new Vector2(0, 800));
            }
            
        }
        
    }
}
