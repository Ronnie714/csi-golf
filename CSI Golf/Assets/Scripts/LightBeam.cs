﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightBeam : MonoBehaviour {
    public GameObject image;
    public Sprite[] images = new Sprite[4];

    // Start is called before the first frame update
    void Start() {
        image.GetComponent<RectTransform>().position = new Vector2(-1500, 250);
        image.GetComponent<Image>().sprite = images[Random.Range(0, 4)];
    }

    // Update is called once per frame
    void Update() {
        image.GetComponent<RectTransform>().position = new Vector2(image.GetComponent<RectTransform>().position.x + 15, image.GetComponent<RectTransform>().position.y);
        if (image.GetComponent<RectTransform>().position.x > 2000) {
            Destroy(gameObject);
        }
    }
}
