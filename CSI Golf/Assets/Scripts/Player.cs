﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public Rigidbody rb;
    [SerializeField]
    public int speed;
    public float accel = 1;
    // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.W)) {
            Debug.Log(rb.velocity.y);
        }
        if (rb.velocity.y > -0.1 && rb.velocity.y < 0.1) {
            accel = 1;
            if (Input.GetKeyDown(KeyCode.W)) {
                rb.AddForce(new Vector2(0, Input.GetAxisRaw("Vertical") * speed));
            }
                
        } else {
            
            accel *= 1.00005f;
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y - accel);
        }
        
        rb.AddForce(new Vector2(Input.GetAxisRaw("Horizontal") * speed / 19, 0));

    }
        
    
}
