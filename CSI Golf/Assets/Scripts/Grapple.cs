﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grapple : MonoBehaviour
{
    public GameObject ball;
    public Material lineMat;

    bool grappled = false;
    Vector3 pivot;
    float length;
    GameObject tether;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (tether != null)
        {
            GameObject.Destroy(tether);
        }
        if (grappled)
        {
            Vector3 start = ball.GetComponent<Rigidbody>().transform.position;
            Vector3 end = pivot;
            tether = new GameObject();
            tether.transform.position = start;
            tether.AddComponent<LineRenderer>();
            LineRenderer lr = tether.GetComponent<LineRenderer>();
            lr.material = lineMat;
            lr.SetColors(Color.white, Color.white);
            lr.SetWidth(0.1f, 0.1f);
            lr.SetPosition(0, start);
            lr.SetPosition(1, end);

            Vector3 diff = ball.GetComponent<Rigidbody>().transform.position - pivot;
            if (diff.magnitude > length)
            {
                ball.GetComponent<Rigidbody>().transform.position = pivot + (diff/diff.magnitude) * length;
                double ang = Vector3.Angle(ball.GetComponent<Rigidbody>().velocity, -diff);
                ball.GetComponent<Rigidbody>().velocity -= Vector3.Project(ball.GetComponent<Rigidbody>().velocity, diff);
            }
        }
    }


    void OnMouseDown()
    {
        if (grappled)
        {
            grappled = false;
        }
        else
        {
            RaycastHit hit;

            Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(ball.GetComponent<Rigidbody>().transform.position);
            dir.z = 0;

            if (Physics.Raycast(ball.GetComponent<Rigidbody>().transform.position, dir, out hit))
            {
                pivot = hit.point;
                length = hit.distance;
                grappled = true;
            }
        }
    }

}
