﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CopLights : MonoBehaviour {
    byte r, g, b;
    Color32 target = Color.blue;
    Color32 current;
    byte speed = 8;
    // Start is called before the first frame update
    void Start() {
        current = target;
        GetComponent<Image>().color = current;
    }

    // Update is called once per frame
    void Update()  {
        if (current.r < target.r) {
            r += speed;
        }
        if (current.r > target.r) {
            r -= speed;
        }
        if (current.g < target.g) {
            g += speed;
        }
        if (current.g > target.g) {
            g -= speed;
        }
        if (current.b < target.b) {
            b += speed;
        }
        if (current.b > target.b) {
            b -= speed;
        }
        current = new Color32(r, g, b, 255);
        GetComponent<Image>().color = current;


        if (Mathf.Abs(current.a - target.a) < speed && Mathf.Abs(current.g - target.g) < speed && Mathf.Abs(current.b - target.b) < speed) {
            
            if (target == Color.red) {
                
                target = Color.blue;
            } else if (target == Color.blue) {
                target = Color.red;
            }
        }
    }
}
