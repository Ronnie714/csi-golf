﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBeams : MonoBehaviour
{
    public GameObject prefab;
    int count = 50;
    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        count--;
        if (count <= 0) {
            count = 50 + (Random.Range(-16, 21));
           GameObject g = Instantiate(prefab);
           g.transform.parent = gameObject.transform;
        }
    }
}
